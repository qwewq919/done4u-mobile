import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  isLogged = false;

  constructor(
    private router: Router,
    private menu: MenuController
  ) { }

  ngOnInit() { }

  closeMenu() {
    this.menu.close('mainMenu');
  }

  logout() {
    this.closeMenu();
    this.router.navigate(['/', 'auth', 'logout']);
  }

  isMenuActivate(status: string): boolean {
    return this.router.url.indexOf(status) !== -1;
  }

  isMenuVisible() {
    if (!this.isLogged) {
      this.isLogged = true;
    }

    const urls = ['home', 'new-orders', 'profile', 'chats'];
    const regex = new RegExp('^\\/(' + urls.join('|') + ')');
    return regex.test(this.router.url);
  }
}
