import { ProfileAddressesComponent } from './components/addresses/addresses.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilePage } from './profile.page';
import { ProfileInfoComponent } from './components/info/info.component';

const routes: Routes = [
  {
    path: '',
    component: ProfilePage,
    children: [
      { path: 'info', component: ProfileInfoComponent },
      { path: 'addresses', component: ProfileAddressesComponent },
      { path: '', redirectTo: 'info', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilePageRoutingModule {}
