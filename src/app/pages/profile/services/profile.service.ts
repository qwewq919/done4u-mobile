import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { IRegion } from '@interfaces/region.interface';
import { IUser } from '@interfaces/user.interface';
import { IUserAddress } from '@interfaces/user-address.interface';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  regionsUrl = `${environment.apiUrl}/regions`;
  usersUrl = `${environment.apiUrl}/users`;

  constructor(
    private http: HttpClient,
  ) { }

  updateProfile(id: number, data: Partial<IUser>): Observable<IUser> {
    return this.http.put<IUser>(`${this.usersUrl}/${id}`, data);
  }

  addAddress(userId: number, data: Partial<IUserAddress>): Observable<IUser> {
    const url = `${environment.apiUrl}/users/${userId}/addresses`;
    return this.http.post<IUser>(url, data);
  }

  updateAddress(userId: number, id: number, data: Partial<IUserAddress>): Observable<IUser> {
    const url = `${environment.apiUrl}/users/${userId}/addresses/${id}`;
    return this.http.put<IUser>(url, data);
  }

  deleteAddress(userId: number, id: number): Observable<IUser> {
    const url = `${environment.apiUrl}/users/${userId}/addresses/${id}`;
    return this.http.delete<IUser>(url);
  }

  getRegions(): Observable<{ data: IRegion[], size: number }> {
    return this.http.post<{ data: IRegion[], size: number }>(`${this.regionsUrl}/list`, {});
  }
}
