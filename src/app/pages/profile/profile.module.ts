import { IonicSelectableModule } from 'ionic-selectable';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Network } from '@ionic-native/network/ngx';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { Connectivity } from '@modules/shared/services/connectivity.service';
import { GoogleMaps } from '@modules/shared/services/google-maps.service';
import { SharedModule } from '@modules/shared/shared.module';
import { ProfileAddressesComponent } from './components/addresses/addresses.component';
import { ProfileInfoComponent } from './components/info/info.component';
import { ProfilePageRoutingModule } from './profile-routing.module';
import { ProfilePage } from './profile.page';
import { ProfileService } from './services/profile.service';

@NgModule({
  declarations: [
    ProfilePage,
    ProfileInfoComponent,
    ProfileAddressesComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    IonicSelectableModule,
    NgxMaskIonicModule,
    ProfilePageRoutingModule,
    SharedModule
  ],
  providers: [
    ProfileService,
    Network,
    Connectivity,
    Geolocation,
    GoogleMaps
  ]
})
export class ProfilePageModule { }
