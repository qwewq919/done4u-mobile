import { takeUntil } from 'rxjs/operators';
import { combineLatest, Subject } from 'rxjs';
import { isEmpty, omitBy } from 'lodash';
import { Platform, ModalController } from '@ionic/angular';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { IRegion } from '@interfaces/region.interface';
import { IUser } from '@interfaces/user.interface';
import { IUserAddress } from '@interfaces/user-address.interface';
import { AddressModalComponent } from '../modals/address/address.component';
import { AuthService } from '@modules/auth/services/auth.service';
import { ProfileService } from '@pages/profile/services/profile.service';
import { StorageService } from '@modules/shared/services/storage/storage.service';
import { LoaderService } from '@modules/shared/services/loader/loader.service';

@Component({
  selector: 'app-profile-addresses',
  templateUrl: './addresses.component.html',
  styleUrls: ['./addresses.component.scss'],
})
export class ProfileAddressesComponent implements OnInit, OnDestroy {
  client: IUser;

  get isMobile() {
    return this.plt.is('hybrid') || this.plt.is('mobile');
  }

  get isDesktop() {
    return this.plt.is('desktop');
  }

  regions: IRegion[] = [];
  private unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private plt: Platform,
    private modal: ModalController,
    private auth: AuthService,
    private profileService: ProfileService,
    private storage: StorageService,
  ) { }

ngOnInit() {
    combineLatest([
      this.auth.isAuth$,
      this.auth.user$
    ])
    .subscribe(async ([isAuth, client]) => {
      if (isAuth && client) {
        this.client = client;
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  async addAddress() {
    const modal = await this.modal.create({
      component: AddressModalComponent,
      componentProps: { regions: this.regions }
    });

    await modal.present();

    modal.onDidDismiss().then(res => {
      if (res && !isEmpty(res.data)) {
        this.profileService
          .addAddress(this.client.id, omitBy(res.data, isEmpty))
          .pipe(takeUntil(this.unsubscribe))
          .subscribe(client => {
            this.client = client;
            this.auth.user$.next(client);
            this.storage.set('currentUser', client);
          });
      }
    });
  }

  async editAddress(address: IUserAddress) {
    const modal = await this.modal.create({
      component: AddressModalComponent,
      componentProps: {
        address,
        regions: this.regions
      }
    });

    await modal.present();

    modal.onDidDismiss().then(res => {
      if (res && !isEmpty(res.data)) {
        this.profileService
          .updateAddress(this.client.id, address.id, omitBy(res.data, isEmpty))
          .pipe(takeUntil(this.unsubscribe))
          .subscribe(client => {
            this.client = client;
            this.auth.user$.next(client);
            this.storage.set('currentUser', client);
          });
      }
    });
  }

  deleteAddress(event: MouseEvent, address: IUserAddress) {
    event.stopImmediatePropagation();

    this.profileService
      .deleteAddress(this.client.id, address.id)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(client => {
        this.client = client;
        this.auth.user$.next(client);
        this.storage.set('currentUser', client);
      });
  }
}
