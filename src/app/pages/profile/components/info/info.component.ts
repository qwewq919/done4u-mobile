import { takeUntil } from 'rxjs/operators';
import { AuthService } from '@modules/auth/services/auth.service';
import { Platform } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IUser } from '@interfaces/user.interface';
import { Subject, combineLatest } from 'rxjs';
import { ProfileService } from '@pages/profile/services/profile.service';
import { omitBy, isEmpty } from 'lodash';
import { StorageService } from '@modules/shared/services/storage/storage.service';
import { LoaderService } from '@modules/shared/services/loader/loader.service';

@Component({
  selector: 'app-profile-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class ProfileInfoComponent implements OnInit, OnDestroy {
  client: IUser;
  clientForm: FormGroup;
  showPassword = false;

  get isMobile() {
    return this.plt.is('hybrid') || this.plt.is('mobile');
  }

  get isDesktop() {
    return this.plt.is('desktop');
  }

  private unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private plt: Platform,
    private fb: FormBuilder,
    private auth: AuthService,
    private service: ProfileService,
    private storage: StorageService,
    private loading: LoaderService,
  ) { }

  ngOnInit() {
    this.clientForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      birthDay: '',
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: '', // @FIXME: add validator when property is ready & got
      phone: [null, [
          Validators.required,
          Validators.pattern(/^\d+$/),
          Validators.maxLength(10),
          Validators.minLength(10),
      ]],
      zip: [''],
    });


    combineLatest([
      this.auth.isAuth$,
      this.auth.user$
    ])
      .subscribe(async ([isAuth, client]) => {
        if (isAuth && client) {
          this.client = client;
          this.clientForm.patchValue(omitBy(client, isEmpty));
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  async save(): Promise<any> {
    if (this.clientForm.invalid) {
      return;
    }

    // const canadaCode = '+1';
    const formData = this.clientForm.getRawValue();
    // formData.phone = formData.phone.replace(canadaCode, '');

    await this.loading.presentLoader();

    const client = await this.service
      .updateProfile(this.client.id, formData)
      .toPromise();

    this.client = client;
    this.auth.user$.next(client);
    await this.storage.set('currentUser', client);
    await this.loading.dismissLoader();
  }
}
