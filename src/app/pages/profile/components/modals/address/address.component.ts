/// <reference types="@types/googlemaps" />

import { IRegion } from '@interfaces/region.interface';
import {
  AfterViewInit, Component, ElementRef, Input, NgZone, OnInit, ViewChild, OnChanges
} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';

import { IUserAddress } from '@interfaces/user-address.interface';
import { GoogleMaps } from '@modules/shared/services/google-maps.service';

@Component({
  selector: 'app-address-modal',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
})
export class AddressModalComponent implements OnInit, OnChanges, AfterViewInit {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;
  @Input() address: IUserAddress;
  @Input() regions: IRegion[];
  addressForm: FormGroup;
  latitude: number;
  longitude: number;
  autocompleteService: any;
  placesService: any;
  places: any = [];
  searchDisabled: boolean;
  saveDisabled: boolean;
  location: any;
  marker: google.maps.Marker;
  // logoSrc = '/assets/images/logo logo.svg';
  logoSrc = '';

  queryControl: FormControl;

  constructor(
    private modal: ModalController,
    private zone: NgZone,
    private geo: Geolocation,
    private maps: GoogleMaps,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.searchDisabled = true;
    this.saveDisabled = true;

    this.addressForm = this.fb.group({
      address: ['', Validators.required],
      region: [''],
      zip: ['', Validators.required],
      lon: '',
      lat: ''
    });

    this.queryControl = new FormControl('');
  }

  ngOnChanges(): void {
    if (this.address) {
      this.addressForm.patchValue(this.address);

      if (this.address.region && this.regions) {
        const regionFoundIndex = this.regions.findIndex(reg => reg.name === this.address.region.code);

        if (regionFoundIndex !== -1) {
          this.addressForm.get('region').setValue(this.address.region.code);
        }
      }
    }
  }

  ngAfterViewInit() {
    this.loadMap();
  }

  loadMap(): void {
    this.maps
      .init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement)
      .then(() => {
        this.autocompleteService = new google.maps.places.AutocompleteService();
        this.placesService = new google.maps.places.PlacesService(this.maps.map);
        this.searchDisabled = false;
        // const geo = await this.geo.getCurrentPosition();
        //
        // const position = {
        //   lat: geo.coords.latitude,
        //   lng: geo.coords.longitude,
        // };
        const ottawaGeoPosition = {
          lat: 45.4215,
          lng: -75.69,
        };

        const ontarioInitialLatLong = new google.maps.LatLng(ottawaGeoPosition.lat, ottawaGeoPosition.lng);

        this.marker = new google.maps.Marker({
          position: ottawaGeoPosition,
          map: this.maps.map,
          title: 'You'
        });

        this.maps.map.setCenter(ontarioInitialLatLong);
        this.setLocation(ontarioInitialLatLong);

        this.maps.map.addListener('click', (e: any) => {
          this.maps.map.setCenter(e.latLng);
          this.marker.setPosition(e.latLng);
          this.setLocation(e.latLng);
        });
      });
  }

  close() {
    this.modal.dismiss();
  }

  save() {
    if (this.addressForm.valid) {
      this.modal.dismiss(this.addressForm.getRawValue());
    }
  }

  selectPlace(place: any) {
    this.places = [];

    this.placesService.getDetails({ placeId: place.place_id }, (details) => {
      this.zone.run(() => {
        this.saveDisabled = false;
        this.maps.map.setCenter(details.geometry.location);
        this.marker.setPosition(details.geometry.location);
        this.setLocation(details.geometry.location);
      });
    });
  }

  searchPlace() {
    this.saveDisabled = true;
    const query = this.queryControl.value;

    if (query.length > 0 && !this.searchDisabled) {
      const config = {
        types: ['geocode'],
        input: query
      };

      this.autocompleteService.getPlacePredictions(config, (predictions: any, status: any) => {
        if (status === google.maps.places.PlacesServiceStatus.OK && predictions) {
          this.places = [];

          predictions.forEach((prediction) => {
            this.places.push(prediction);
          });
        }
      });
    } else {
      this.places = [];
    }
  }

  private setLocation(loc: google.maps.LatLng) {
    const geoCoder = new google.maps.Geocoder();

    geoCoder.geocode({ location: loc }, (results: google.maps.GeocoderResult[]) => {
      const location = results[0];
      const postalCode = location.address_components.find(ac => ac.types.includes('postal_code'));
      const regionCode = location.address_components.find(ac => ac.types.includes('administrative_area_level_1'));
      const regionFoundInList = this.regions.find(reg => reg.code === regionCode.short_name);

      this.addressForm.patchValue({
        address: location.formatted_address,
        region: regionFoundInList ? regionCode.short_name : null,
        zip: postalCode ? postalCode.long_name : null,
        lon: location.geometry.location.lng(),
        lat: location.geometry.location.lat(),
      });
    });
  }
}
