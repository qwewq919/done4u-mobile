import { findIndex, find } from 'lodash';
import moment from 'moment';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ElementRef, DoCheck } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IChatMessage } from '@interfaces/chat-message.interface';
import { AuthService } from '@modules/auth/services/auth.service';
import { ChatService } from '@modules/shared/services/chat/chat.service';
import { SocketService } from '@modules/shared/services/socket/socket.service';
import { IOrder } from '@interfaces/order.interface';
import { OrdersService } from '@modules/shared/services/orders/orders.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {
  orderId: number;
  user: number;
  @ViewChild('scrollMe') scrollContainer: ElementRef;
  messages: IChatMessage[] = [];
  messageCtrl: FormControl = new FormControl('');
  order: IOrder;

  private unsubscribe: Subject<any> = new Subject<any>();

  get currentUserId(): number {
    return this.auth.user$.value.id;
  }

  constructor(
    private socket: SocketService,
    private chat: ChatService,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    public auth: AuthService,
    private orderService: OrdersService,
  ) { }


  ngOnInit(): void {
    this.route.paramMap
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(params => {
        this.orderId = parseInt(params.get('orderId'), 10);
        this.socket.initSocket();
        this.user = this.auth.user$.value.id;
        this.loadOrder();

        this.socket.onChatMessage
          .pipe(takeUntil(this.unsubscribe))
          .subscribe((message: IChatMessage) => {
            const isMessageNotFound = this.orderId
              && message.order.id === this.orderId
              && !find(this.messages, m => m.id === message.id);

            if (isMessageNotFound) {
              const idx = findIndex(this.messages, m => moment(m.createdAt)
                .isAfter(moment(message.createdAt)));

              if (idx !== -1) {
                this.messages.splice(idx - 1, 1, message);
              } else {
                this.messages.push(message);
              }

              this.cdr.detectChanges();
            }
          });

        this.chat
          .getMessages(this.orderId)
          .pipe(takeUntil(this.unsubscribe))
          .subscribe(messages => {
            this.messages = messages;

          });
      });
  }
  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  sendMessageWithButton(): void {
    this.sendMessage();
  }

  sendMessageWithKeyboard(event: KeyboardEvent) {
    if (event.ctrlKey && event.code === 'Enter') {
      this.sendMessage();
    }
  }

  private sendMessage() {
    const messageText = this.messageCtrl.value;

    if (!messageText || !messageText.trim()) {
      return;
    }

    this.chat
      .sendMessage({
        message: messageText,
        from: this.user as any,
        order: this.orderId as any,
      })
      .subscribe(message => {
        const idx = findIndex(
          this.messages,
          m => moment(m.createdAt).isAfter(moment(message.createdAt))
        );

        if (idx !== -1) {
          this.messages.splice(idx - 1, 1, message);
        } else {
          this.messages.push(message);
        }

        this.cdr.detectChanges();
        this.messageCtrl.patchValue('');
        this.scrollContainer.nativeElement.scrollIntoView({ behavior: 'auto', block: 'end' });
      });
  }


  private loadOrder(): void {
    this.orderService
      .getOrderById(this.orderId)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(order => this.order = order);
  }
}
