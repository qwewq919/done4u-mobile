import { AuthService } from '@modules/auth/services/auth.service';
import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { IChatMessage } from '@interfaces/chat-message.interface';

@Component({
  selector: 'app-chat-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class ChatMessageComponent {
  @Input() message: IChatMessage;
  @Input() isOwnMessage: boolean;

  get userName(): string {
    const user = this.message.from;
    const fName = user.firstName ? `${user.firstName} ` : '';
    const lName = user.lastName ?? '';
    return `${fName}${lName}`;
  }
}
