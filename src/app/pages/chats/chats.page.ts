import { plainToClass } from 'class-transformer';
import { takeUntil, map } from 'rxjs/operators';
import { combineLatest, Subject } from 'rxjs';
import { IOrder } from '@interfaces/order.interface';
import { OrdersService } from '@modules/shared/services/orders/orders.service';
import { AuthService } from '@modules/auth/services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoaderService } from '@modules/shared/services/loader/loader.service';
import { ImageService } from '@modules/shared/services/image/image.service';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.page.html',
  styleUrls: ['./chats.page.scss'],
})
export class ChatsPage implements OnInit, OnDestroy {
  orders: IOrder[] = [];

  logoSrc = '/assets/images/logo.png';
  logoSrcMidiChat = '/assets/images/mdi_chat2.svg';

  statuses = {
    created: 'Waiting',
    'in-progress': 'In Progress',
    completed: 'Completed',
    canceled: 'Canceled'
  };

  private unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private auth: AuthService,
    private service: OrdersService,
    private imageService: ImageService,
  ) { }

  ngOnInit() {
    combineLatest([
      this.auth.isAuth$,
      this.auth.user$
    ])
      .subscribe(([isAuth, client]) => {
        if (isAuth && client) {
          this.loadOrders(client.id);
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  private loadOrders(clientId: number) {
    this.service
      .getOrders(clientId)
      .pipe(
        map(res => res.data),
        map(orders => plainToClass(IOrder, orders)),
        takeUntil(this.unsubscribe)
      )
      .subscribe(orders => this.orders = orders);
  }

  getServiceIcon(serviceName: string): string {
    return this.imageService.getServiceIcon(serviceName);
  }
}
