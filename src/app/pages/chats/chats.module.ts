import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SharedModule } from '@modules/shared/shared.module';
import { ChatsPageRoutingModule } from './chats-routing.module';
import { ChatsPage } from './chats.page';
import { ChatComponent } from './components/chat/chat.component';
import { ChatMessageComponent } from './components/chat/components/message/message.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    ChatsPageRoutingModule
  ],

  declarations: [
    ChatsPage,
    ChatComponent,
    ChatMessageComponent,
  ]
})
export class ChatsPageModule { }
