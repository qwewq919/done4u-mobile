import { Component, OnDestroy, OnInit } from '@angular/core';
import { IOrder } from '@interfaces/order.interface';
import { IService } from '@interfaces/service.interface';
import { AuthService } from '@modules/auth/services/auth.service';
import { ImageService } from '@modules/shared/services/image/image.service';
import { LoaderService } from '@modules/shared/services/loader/loader.service';
import { OrdersService } from '@modules/shared/services/orders/orders.service';
import { plainToClass } from 'class-transformer';
import { combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {
  logoSrc = '/assets/images/logo.png';
  logoSrcMidiChat = '/assets/images/mdi_chat.svg';

  services: IService[] = [];
  orders: IOrder[] = [];
  orderRelevant: IOrder[] = [];
  orderDrafts: IOrder[] = [];
  orderCompleted: IOrder[] = [];

  taxAmount = 0.13;
  step: number;

  statuses = {
    draft: 'Draft',
    created: 'Waiting',
    'in-progress': 'In Progress',
    completed: 'Completed',
    canceled: 'Canceled',
    archive: 'Archive'
  };

  segment: string;
  allowedSegmentTabs = ['Relevant', 'Drafts', 'Completed'];

  private unsubscribe: Subject<any> = new Subject<any>();

  private orderCompletedStatuses = [
    'completed',
    'archive',
    'canceled',
  ];

  private relevantOrderStatuses = [
    'in-progress',
    'created',
  ];

  get orderDraftsLength(): number {
    return this.orderDrafts && this.orderDrafts.length;
  }

  get orderRelevantLength(): number {
    return this.orderRelevant && this.orderRelevant.length;
  }

  get orderCompletedLength(): number {
    return this.orderCompleted && this.orderCompleted.length;
  }

  constructor(
    private auth: AuthService,
    private loading: LoaderService,
    private service: OrdersService,
    private imageService: ImageService,
  ) {
    this.step = 1;
    this.segment = 'Relevant';
  }

  ngOnInit() {
    this.step = 1;
    this.loadServices();

    combineLatest([
      this.auth.isAuth$,
      this.auth.user$
    ])
      .subscribe(([isAuth, client]) => {
        if (isAuth && client) {
          this.loadOrders(client.id);
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  calcTax(amount: number): number {
    return amount * this.taxAmount;
  }

  private async loadServices() {
    await this.loading.presentLoader();
    this.service
      .getServices()
      .pipe(
        takeUntil(this.unsubscribe),
        map(res => plainToClass(IService, res.data))
      )
      .subscribe(async services => {
        this.services = services;
        await this.loading.dismissLoader();
      });
  }

  private loadOrders(clientId: number) {
    this.service
      .getOrders(clientId)
      .pipe(
        takeUntil(this.unsubscribe),
        map(res => plainToClass(IOrder, res.data))
      )
      .subscribe(orders => {
        this.orders = orders;
        this.SortLoadOrder();
      });
  }

  private SortLoadOrder() {

    this.orderRelevant = this.orders.filter(item => this.relevantOrderStatuses.indexOf(item.status) !== -1);
    this.orderCompleted = this.orders.filter(item => this.orderCompletedStatuses.indexOf(item.status) !== -1);
    // this.orderDrafts = this.orders.filter(item => item.status === 'draft');
  }

  segmentChanged(ev: any) {
    const tab = ev.detail.value;
    switch (tab) {
      case 'Relevant': this.step = 1; break;
      case 'Drafts': this.step = 2; break;
      case 'Completed': this.step = 3; break;
    }
  }

  getServiceIcon(serviceName: string): string {
    return this.imageService.getServiceIcon(serviceName);
  }
}
