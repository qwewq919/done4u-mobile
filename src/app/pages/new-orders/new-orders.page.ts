import { Component, OnInit, OnDestroy } from '@angular/core';
import { IOrder } from '@interfaces/order.interface';
import { IService } from '@interfaces/service.interface';
import { Subject } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { OrdersService } from '@modules/shared/services/orders/orders.service';
import { plainToClass } from 'class-transformer';
import { LoaderService } from '@modules/shared/services/loader/loader.service';
import { ImageService } from '@modules/shared/services/image/image.service';

@Component({
  selector: 'app-new-orders',
  templateUrl: './new-orders.page.html',
  styleUrls: ['./new-orders.page.scss'],
})
export class NewOrdersPage implements OnInit, OnDestroy {
  services: IService[] = [];
  orders: IOrder[] = [];
  empty = 'Description';

  private taxAmount = 0.13;
  private unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private service: OrdersService,
    private loading: LoaderService,
    private imageService: ImageService,
  ) { }

  ngOnInit() {
    this.loadServices();
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  calcTax(amount: number): number {
    return amount * this.taxAmount;
  }

  private async loadServices() {
    await this.loading.presentLoader();
    this.service
      .getServices()
      .pipe(
        takeUntil(this.unsubscribe),
        map(res => plainToClass(IService, res.data))
      )
      .subscribe(async services => {
        this.services = services;
        await this.loading.dismissLoader();
      });
  }

  getServiceIcon(serviceName: string): string {
    return this.imageService.getServiceIcon(serviceName);
  }
}

