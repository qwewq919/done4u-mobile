import { IonicModule } from '@ionic/angular';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NewOrdersPageRoutingModule } from './new-orders-routing.module';
import { NewOrdersPage } from './new-orders.page';

@NgModule({
  declarations: [
    NewOrdersPage
  ],

  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewOrdersPageRoutingModule
  ],
})
export class NewOrdersPageModule { }
