import { OrderFooterComponent } from './components/footer/footer.component';
import { OrderHeaderComponent } from './components/header/header.component';
import { RatingModule } from 'ng-starrating';
import { IonicModule } from '@ionic/angular';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { OrderPageRoutingModule } from './order-routing.module';
import { OrderPage } from './order.page';
import { SharedModule } from '@modules/shared/shared.module';

@NgModule({
  declarations: [
    OrderPage,
    OrderHeaderComponent,
    OrderFooterComponent,
  ],

  entryComponents: [],

  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    RatingModule,
    OrderPageRoutingModule,
    SharedModule
  ],

  providers: [
    // DatePipe
  ]
})
export class OrderPageModule {}
