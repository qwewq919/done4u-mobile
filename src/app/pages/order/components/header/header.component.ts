import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-order-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class OrderHeaderComponent implements OnInit {
  @Input() checkSelectorsOptions = true;
  @Input() checkSelectorsAddress = false;
  @Input() checkSelectorsDate = false;
  @Input() step = 1;
  @Input() type: any;
  @Input() selectedPrice: any;
  @Input() order: any;
  @Input() logoSrc: string;

  @Output() stepChange = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  goBackToNewOrder() {
    this.step = 2;
    this.stepChange.emit(2);
  }
}
