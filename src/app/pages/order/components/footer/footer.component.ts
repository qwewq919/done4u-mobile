import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-order-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class OrderFooterComponent implements OnInit {
  @Input() checkSelectorsOptions = true;
  @Input() checkSelectorsAddress = false;
  @Input() checkSelectorsDate = false;
  @Input() checkSelectorsOptionsButtons = false;
  @Input() step = 1;
  @Input() type: any;
  @Input() selectedPrice: any;
  @Input() orderForm: FormGroup;
  @Input() client: any;
  @Input() cardHolder: any;
  @Input() isCardError = false;
  @Input() order: any;
  @Input() continue: () => void;
  @Input() stripeCharge: () => void;
  @Input() getAddress: () => any;
  @Input() pay: (event: any) => void;

  @Output() checkSelectorsOptionsChange = new EventEmitter();
  @Output() checkSelectorsAddressChange = new EventEmitter();
  @Output() checkSelectorsDateChange = new EventEmitter();
  @Output() stepChange = new EventEmitter();
  @Output() cancel = new EventEmitter();

  @HostBinding('class.green-buttons-pole') hostClass = true;

  constructor() { }

  ngOnInit() {}

  showOptions() {
    this.checkSelectorsOptions = true;
    this.checkSelectorsAddress = false;
    this.checkSelectorsOptionsChange.emit(true);
    this.checkSelectorsAddressChange.emit(false);
  }

  hideOptions() {
    this.checkSelectorsOptions = false;
    this.checkSelectorsAddress = true;
    this.checkSelectorsOptionsChange.emit(false);
    this.checkSelectorsAddressChange.emit(true);
  }

  hideAddres() {
    this.checkSelectorsAddress = false;
    this.checkSelectorsDate = true;
    this.checkSelectorsAddressChange.emit(false);
    this.checkSelectorsDateChange.emit(true);
  }

  hideDate() {
    this.checkSelectorsAddress = true;
    this.checkSelectorsDate = false;
    this.checkSelectorsAddressChange.emit(true);
    this.checkSelectorsDateChange.emit(false);
  }

  backPaymentToDataModal() {
    this.step = 1;
    this.checkSelectorsAddress = false;
    this.checkSelectorsDate = true;
    this.stepChange.emit(1);
    this.checkSelectorsAddressChange.emit(false);
    this.checkSelectorsDateChange.emit(true);
  }

  goBackToNewOrder() {
    this.step = 2;
    this.stepChange.emit(2);
  }

  cancelOrder() {
    this.cancel.emit();
  }
}
