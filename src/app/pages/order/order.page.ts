import { IOrderPayment } from '@interfaces/order-payment.interface';
import { plainToClass } from 'class-transformer';
import { AlertController, ModalController } from '@ionic/angular';
import { isEmpty, omit, omitBy } from 'lodash';
import moment from 'moment';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { map, pluck, takeUntil, tap, switchMap } from 'rxjs/operators';
import { loadStripe, Stripe, StripeCardElement, StripeElements } from '@stripe/stripe-js';

import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { environment } from '@environments/environment';
import { IOrder, OrderStatus } from '@interfaces/order.interface';
import { IOrderSchedule } from '@interfaces/orderSсhedule.interface';
import { IRegion } from '@interfaces/region.interface';
import { IServicePrice } from '@interfaces/service-price.interface';
import { IService } from '@interfaces/service.interface';
import { IUser } from '@interfaces/user.interface';
import { AuthService } from '@modules/auth/services/auth.service';
import { GoogleMaps } from '@modules/shared/services/google-maps.service';
import { ImageService } from '@modules/shared/services/image/image.service';
import { LoaderService } from '@modules/shared/services/loader/loader.service';
import { OrdersService } from '@modules/shared/services/orders/orders.service';
import { StorageService } from '@modules/shared/services/storage/storage.service';
import { AddressModalComponent } from '@pages/profile/components/modals/address/address.component';
import { ProfileService } from '@pages/profile/services/profile.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('paymentForm') paymentForm: ElementRef;
  @ViewChild('card' ) stripeCard: ElementRef;
  @ViewChild('cardErrors') cardErrors: ElementRef;
  @ViewChild('map', { static: false }) mapElement: ElementRef;
  @ViewChild('pleaseConnect', { static: false }) pleaseConnect: ElementRef;

  cardHolder: FormControl = new FormControl('', Validators.required);
  rating: FormControl = new FormControl(0);
  ratingComment: FormControl = new FormControl('');
  orderForm: FormGroup;
  all: FormGroup;

  logoSrc = '/assets/images/ssww.png';
  logoSrcPayloadShild = '/assets/images/shield-checkmark-outline.png';
  logoSrcPayloadShildIcnon = '/assets/images/shield-checkmark.svg';

  client: IUser;

  services: IService[] = [];
  prices: IServicePrice[] = [];
  regions: IRegion[] = [];
  orderSchedule: IOrderSchedule[] = [];
  schedules: IOrderSchedule[] = [];

  type: IService;
  order: IOrder;
  payments: IOrderPayment[] = [];
  tax = 0;
  step = 1;

  test;

  selectedPrice: any;

  checkSelectorsOptionsButtons = false;
  checkSelectorsOptions = true;
  checkSelectorsAddress = false;
  checkSelectorsDate = false;

  stripe: Stripe;
  stripeElements: StripeElements;
  hasShowMap$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  statuses = {
    draft: 'Draft',
    created: 'Waiting',
    'in-progress': 'In Progress',
    completed: 'Completed',
    canceled: 'Canceled'
  };

  isCardError = true;
  nowDate: string;

  private card: StripeCardElement;
  private unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private auth: AuthService,
    private fb: FormBuilder,
    private imageService: ImageService,
    private loading: LoaderService,
    private maps: GoogleMaps,
    private modal: ModalController,
    private ordersService: OrdersService,
    private profileService: ProfileService,
    private route: ActivatedRoute,
    private router: Router,
    private storage: StorageService,
    public alertCtrl: AlertController,
  ) {
    this.nowDate = new Date().toISOString();
    this.continue = this.continue.bind(this);
    this.stripeCharge = this.stripeCharge.bind(this);
    this.pay = this.pay.bind(this);
  }

  ngOnInit() {
    combineLatest([
      this.auth.isAuth$,
      this.auth.user$,
      this.loadServices(),
    ])
    .pipe(takeUntil(this.unsubscribe))
    .subscribe(async ([isAuth, client, services]) => {
      await this.loading.presentLoader();

      if (isAuth && client) {
        this.client = client;
        this.loadOrderAndSchedule();
        this.createFormForTypeParam(client);
        this.createFormForBaseParam(client);
      }

      await this.loading.dismissLoader();
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  ngAfterViewInit() {
    // console.log('he i tut');
    
    // const style = {
    //   base: {
    //     fontSize: '16px',
    //     color: '#32325d',
    //   },
    // };
    // this.card = this.stripeElements.create('card', { style });
    // this.card.mount(this.stripeCard.nativeElement);
    // this.card.on('change', (event) => {
    //   const displayError = this.cardErrors.nativeElement;
    //   if (event.error) {
    //     displayError.textContent = event.error.message;
    //   } else {
    //     displayError.textContent = '';
    //   }
    //   this.isCardError = !event.complete;
    // });
    
    // this.initStripe()
    if (!this.mapElement) {
      return;
    }

    this.hasShowMap$
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(loaded => {
        if (loaded) {
          setTimeout(() => this.loadMap(), 100);
        }
      });
  }

  async continue() {
    const dateStr = moment(this.orderForm.get('expiredAt').value).format('YYYY-MM-DD');
    const timeStr = moment(this.orderForm.get('expiredHours').value).format('HH:mm');
    const date = moment(`${dateStr}T${timeStr}`).utcOffset(moment().utcOffset()).toDate();

    this.orderForm.get('expiredAt').setValue(date);

    if (this.orderForm.get('address').value === 'new') {
      await this.createAddress();
      this.hasShowMap$.next(true);
    } else {
      this.step = 2;
      console.log(this.orderForm.value);
      
      this.hasShowMap$.next(true);
    }
    this.checkSelectorsAddress = false;
    this.checkSelectorsDate = false;
  }

  logout() {
    this.router.navigate(['/', 'auth', 'logout']);
  }

  toggleOption(id: string) {
    if (this.selectedPrice && this.selectedPrice.id === id) {
      this.selectedPrice = null;
    } else {
      this.selectedPrice = this.prices.find(o => o.id === parseInt(id, 10));
    }

    this.orderForm.get('price').patchValue(this.selectedPrice ? this.selectedPrice.id : null);
  }

  isSelected(id: string): boolean {
    return this.selectedPrice && this.selectedPrice.id === id;
  }

  getAddressStr() {
    const addrVal = this.orderForm.get('address').value;
    const found = this.client.addresses.find(a => a.id === addrVal);
    return found ? found.address : '';
  }

  getDateOrder() {
    const expiredAt = this.orderForm.get('expiredAt').value;
    return new DatePipe('en_CA').transform(expiredAt, 'EEEE, MMMM d y  HH:mm');

  }

  getAddress() {
    const addrVal = this.orderForm.get('address').value;
    return this.client.addresses.find(a => a.id === addrVal);
  }

  async createAddress() {
    const addr = this.orderForm.get('addressStr').value;

    return new Promise(async resolve => {
      this.ordersService
        .createAddress(this.client.id, addr)
        .pipe(takeUntil(this.unsubscribe))
        .subscribe(client => {
          this.client = client;
          const addrId = client.addresses.find(a => a.address === addr).id;
          this.orderForm.get('address').patchValue(addrId);
          this.orderForm.get('addressStr').patchValue(null);
          this.auth.user$.next(client);

          this.storage
            .set('currentUser', client)
            .then(() => {
              this.step = 2;
              console.log(this.orderForm.value);
              resolve();
            });
        });
      await this.loading.dismissLoader();
    });
  }

  stripeCharge() {
    console.log(this.orderForm.value);
    
    this.step = 4;  
    if (!this.client.paymentMethodId) {
      setTimeout(() => {
        this.initStripe()
      }, 50);
    } else {
      this.pay();
    }
  }

  initStripe(){
    
    const style = {
      base: {
        fontSize: '16px',
        color: '#32325d',
      },
    };
    this.card = this.stripeElements.create('card', { style });
    this.card.mount(this.stripeCard.nativeElement);
    this.card.on('change', (event) => {
      const displayError = this.cardErrors.nativeElement;
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
      this.isCardError = !event.complete;
    });
  
  }

  async pay(event?: MouseEvent | TouchEvent) {
   
    
    this.test = this.orderForm.value;
    this.test.price = this.selectedPrice.id ;
    console.log('yahoo',this.test.price );
    await this.loading.presentLoader();
    console.log('pay',this.orderForm.value);
    console.log(this.orderForm.valid);

    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    if (this.client.paymentMethodId) {
      console.log(this.orderForm.value);
      this.createOrder(this.client.paymentMethodId);
    } else {
      if (this.card && this.cardHolder.valid && !this.isCardError) {
        const result = await this.stripe.createPaymentMethod({
          type: 'card',
          card: this.card,
          billing_details: {
            name: this.cardHolder.value,
          }
        });
        console.log(this.orderForm.value);
        if (result.error) {
          await this.loading.dismissLoader();
          const errorElement = this.cardErrors.nativeElement;
          errorElement.textContent = result.error.message;
        } else {
          this.profileService
            .updateProfile(this.client.id, { paymentMethodId: result.paymentMethod.id })
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(async user => {
              console.log(this.orderForm.value);
              this.auth.user$.next(user);
              await this.storage.set('currentUser', user);
              this.createOrder(user.paymentMethodId);
            });
        }
      }
    }
  }

  createOrder(methodId: string) {    
    // console.log(this.orderForm.value);
    // if (this.orderForm.valid) {
      const formValue = omit(this.test, ['expiredHours']);
      this.ordersService
        .createOrder({
          ...formValue,
          methodId
        } as any)
        .pipe(takeUntil(this.unsubscribe))
        .subscribe(async order => {
          console.log('it oke');
          this.router.navigate(['/', 'orders', order.id]);
          await this.loading.dismissLoader();
        });
    // }
  }

  onRate(event: any) {
    this.order.rating = event.newValue;
  }

  setRating() {
    this.ordersService
      .updateOrder(this.order.id, {
        rating: this.order.rating,
        ratingComment: this.ratingComment.value
      })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(order => this.order = order);
  }

  goBackToNewOrder() {
    this.step = 2;
    
  }

  async addAddress() {
    this.regions = await this.profileService
      .getRegions()
      .pipe(
        takeUntil(this.unsubscribe),
        map(res => plainToClass(IRegion, res.data))
      )
      .toPromise();

    const modal = await this.modal.create({
      component: AddressModalComponent,
      componentProps: { regions: this.regions }
    });

    await modal.present();

    modal
      .onDidDismiss()
      .then(res => {
        if (res && !isEmpty(res.data)) {
          this.profileService
            .addAddress(this.client.id, omitBy(res.data, isEmpty))
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(client => {
              this.client = client;
              this.auth.user$.next(client);
              this.storage.set('currentUser', client);
            });
        }
      });
  }

  getServiceIcon(serviceName: string): string {
    return this.imageService.getServiceIcon(serviceName);
  }

  async presentActionSheet() {
    const prompt = await this.alertCtrl.create({
      header: 'Enter actual hours',
      inputs: [
        {
          name: 'hoursOrder',
          type: 'number',
          placeholder: 'Number of hours'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {

          }
        },
        {
          text: 'OK',
          handler: data => {
            this.actialHourly(data);
            this.presentAlertOrderHours();
          }
        }
      ]
    });

    await prompt.present();
  }

  actialHourly(data: any) {
    if (!data || !data.hoursOrder || isNaN(Number(data.hoursOrder))) {
      return;
    }

    this.loading.presentLoader();

    this.ordersService
      .updateOrder(this.order.id, { factHours: Math.ceil(Number(data.hoursOrder)) })
      .pipe(
        takeUntil(this.unsubscribe),
        tap(order => this.order = order),
        switchMap(() => this.ordersService.getPaymentsByOrder(this.order.id)),
        tap(payments => this.payments = payments),
        tap(() => this.loading.dismissLoader())
      )
      .subscribe(() => {
        this.tax = Math.round(
          this.order.price.amount *
          this.order.factHours *
          (this.order.address.region ? this.order.address.region.taxRate / 100 : 0)
        );
      });
  }

  async presentAlertOrderHours() {
    const  orderTitle = `Order #${this.order.id}`;
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: orderTitle,
      message: 'Total pay has been changed',
      buttons: ['OK']
    });

    await alert.present();
  }

  cancelOrder() {
    this.loading.presentLoader();

    this.ordersService
      .updateOrder(this.order.id, { status: OrderStatus.Canceled })
      .pipe(
        takeUntil(this.unsubscribe),
        tap(order => this.order = order),
        switchMap(() => this.ordersService.getPaymentsByOrder(this.order.id)),
        tap(payments => this.payments = payments),
        tap(() => this.loading.dismissLoader())
      )
      .subscribe();
  }

  private createFormForBaseParam(client): void {
    this.route.queryParams
      .pipe(
        takeUntil(this.unsubscribe),
        pluck('base')
      )
      .subscribe(async base => {
        if (base) {
          this.ordersService
            .getOrderById(base)
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(async order => {
              this.stripe = await loadStripe(environment.stripe.publishKey);
              this.stripeElements = this.stripe.elements();

              this.orderForm = this.fb.group({
                client: '',
                price: ['', Validators.required],
                address: ['', Validators.required],
                addressStr: { value: '', disabled: true },
                expiredAt: ['', Validators.required],
                expiredHours: ['', Validators.required],
                description: '',
              });

              this.orderForm.get('address').valueChanges
                .pipe(takeUntil(this.unsubscribe))
                .subscribe(value => {
                  if (value) {
                    if (value === 'new') {
                      this.orderForm.get('addressStr').setValidators(Validators.required);
                      this.orderForm.get('addressStr').enable();
                    } else {
                      this.orderForm.get('addressStr').clearValidators();
                      this.orderForm.get('addressStr').disable();
                    }
                  }
                });

              this.orderForm.get('client').patchValue(client.id);
              this.type = order.price.service;

              this.loadPrices(order.price.id)
                .pipe(takeUntil(this.unsubscribe))
                .subscribe(() => {
                  this.selectedPrice = order.price;
                  this.orderForm.patchValue({
                    price: order.price.id,
                    address: order.address.id,
                  });
                });
            });
        }
      });
  }

  private loadOrderSсhedule(orderId: number) {
    return this.ordersService
      .getOrderSсheduleById(orderId)
      .pipe(
        takeUntil(this.unsubscribe),
        tap(orderSсhedule => {
          this.orderSchedule = orderSсhedule;
          this.hasShowMap$.next(true);
        })
      )
      .subscribe((value: IOrderSchedule[]) => this.schedules = [...value]);
  }

  private loadOrderAndSchedule(): void {
    this.route.params
      .pipe(
        takeUntil(this.unsubscribe),
        pluck('id'),
      )
      .subscribe(id => {
        if (id) {
          combineLatest([
            this.ordersService.getOrderById(id),
            this.ordersService.getPaymentsByOrder(id)
          ])
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(([ order, payments ]) => {
              this.order = order;
              this.payments = payments;

              if (this.order.price.paymentType === 'fixed') {
                this.tax = Math.round(this.order.price.amount * (this.order.address.region ? this.order.address.region.taxRate / 100 : 0));
              } else if (order.factHours) {
                this.tax = Math.round(
                  this.order.price.amount *
                  this.order.factHours *
                  (this.order.address.region ? this.order.address.region.taxRate / 100 : 0)
                );
              }

              this.hasShowMap$.next(true);
            });

          this.loadOrderSсhedule(id);
        }
      });
  }

  private createFormForTypeParam(client): void {
    this.route.queryParams
    .pipe(
      takeUntil(this.unsubscribe),
      pluck('type')
    )
    .subscribe(async t => {
      if (t) {
        this.stripe = await loadStripe(environment.stripe.publishKey);

        this.stripeElements = this.stripe.elements();

        this.orderForm = this.fb.group({
          client: '',
          price: ['', Validators.required],
          address: ['', Validators.required],
          addressStr: { value: '', disabled: true },
          expiredAt: ['', Validators.required],
          expiredHours: ['', Validators.required],
          description: '',
        });

        this.orderForm.get('address').valueChanges
          .pipe(takeUntil(this.unsubscribe))
          .subscribe(value => {
            if (value) {
              if (value === 'new') {
                this.orderForm.get('addressStr').setValidators(Validators.required);
                this.orderForm.get('addressStr').enable();
              } else {
                this.orderForm.get('addressStr').clearValidators();
                this.orderForm.get('addressStr').disable();
              }
            }
          });

        this.orderForm.get('client').patchValue(client.id);
        this.type = this.services.find(ot => ot.id === parseInt(t, 10));
        this.loadPrices(t).pipe(takeUntil(this.unsubscribe)).subscribe();
      }
    });
  }

  private loadMap(): void {
    this.maps
      .init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement)
      .then(async () => {
        const geoCoder = new google.maps.Geocoder();
        let address: string;

        if (this.order) {
          address = this.order.address.address;
        } else {
          const a = this.client.addresses.find(addr => addr.id === this.orderForm.get('address').value);
          address = a.address;
        }

        geoCoder.geocode(
          { address: this.order.address.address },
          (results: google.maps.GeocoderResult[]) => {
            const location = results[0];
            this.maps.map.setCenter(location.geometry.location);

            const marker = new google.maps.Marker({
              position: location.geometry.location,
              map: this.maps.map,
              title: 'Order address'
            });
          }
        );
      });
  }

  private loadServices() {
    return this.ordersService
      .getServices()
      .pipe(
        takeUntil(this.unsubscribe),
        map(res => res.data),
        map(services => plainToClass(IService, services)),
        tap(async services => { this.services = services; await this.loading.dismissLoader(); })
      );

  }

  private loadPrices(serviceId: number) {
    return this.ordersService
      .getServicePrices(serviceId)
      .pipe(
        takeUntil(this.unsubscribe),
        map(res => res.data),
        map(prices => plainToClass(IServicePrice, prices)),
        tap(prices => {
          this.prices = prices;
          this.checkSelectorsOptionsButtons = true;
        })
      );
  }
}

