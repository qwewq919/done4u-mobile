import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';

import { AuthModule } from '@modules/auth/auth.module';
import { SharedModule } from '@modules/shared/shared.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MainComponent } from '@pages/main.component';
import { AddressModalComponent } from '@pages/profile/components/modals/address/address.component';

@NgModule({
  bootstrap: [
    AppComponent
  ],

  declarations: [
    AppComponent,
    MainComponent,
    AddressModalComponent
  ],

  entryComponents: [AddressModalComponent],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    NgxMaskIonicModule.forRoot(),
    AppRoutingModule,
    AuthModule,
    SharedModule,
  ],

  providers: [
    StatusBar,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
})
export class AppModule {}
