import { IUserAddress } from './user-address.interface';
import { IUser } from './user.interface';
import { IServicePrice } from './service-price.interface';

export enum OrderStatus {
  Created = 'created',
  InProgress = 'in-progress',
  Completed = 'completed',
  Canceled = 'canceled',
  Draft = 'draft',
  Active = 'active',
  Schedule = 'schedule',
  Archive = 'archive'
}

export class IOrder {
  id?: number;
  createdAt: Date;
  expiredAt: Date;
  dateFrom: Date;
  dateTo: Date;
  mapPoint: any;
  description: string;
  status: OrderStatus;
  amount: number; // sum in cents
  prepayId: string;
  paymentId: string;
  rating: number;
  ratingComment: string;
  execution: number;
  factHours: number;
  price: IServicePrice;
  client: IUser;
  contractor: IUser;
  address: IUserAddress;
}
