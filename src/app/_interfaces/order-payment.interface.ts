import { IOrder } from '@interfaces/order.interface';

type PaymentType =
  | 'prepay'
  | 'partial'
  | 'fixed'
  | 'refund';

export class IOrderPayment {
  id?: number;
  status: string;
  description: string;
  summ: number;
  type: PaymentType;
  createdAt: Date;
  order: IOrder;
}
