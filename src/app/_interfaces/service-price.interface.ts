import { IService } from './service.interface';
import { IRegion } from './region.interface';

type PaymentType =
  | 'hourly'
  | 'fixed';

export class IServicePrice {
  id?: number;
  name: string;
  startdate?: Date;
  amount: number;
  status: string;
  description: string;
  deleted: boolean;
  service: IService;
  region: IRegion;
  paymentType: PaymentType;
  prepayment: number;
  position: number;
}
