import { IUser } from './user.interface';

export enum PaymentType {
  In = 'in',
  Out = 'out'
}

export enum PaymentStatus {
  Draft = 'draft',
  Confirmed = 'confirmed',
  Declined = 'declined',
  Canceled = 'canceled'
}

export class IUserPayment {
  id?: number;
  date: Date;
  type: PaymentType;
  amount: number;
  status: PaymentStatus;
  comment: string;
  description: string;
  createdBy: IUser;
  approvedBy: IUser;
}
