import { IUserAddress } from './user-address.interface';
import { IUserPayment } from './user-payment.interface';

export enum UserRole {
  SuperAdmin = 'superadmin',
  Admin = 'admin',
  Client = 'client'
}

export class IUser  {
  id?: number;
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
  password: string;
  resetToken: string;
  role: UserRole;
  status: string;
  birthday: Date;
  createdAt: Date;
  updatedAt: Date;
  addresses: IUserAddress[];
  payments: IUserPayment[];
  zip: string;
  paymentMethodId: string;
  stripeCustomerId: string;

  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }
}
