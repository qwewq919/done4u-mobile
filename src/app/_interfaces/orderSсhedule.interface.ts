export class IOrderSchedule {
  id?: number;
  orderId: number;
  dateSchedule: Date;
  check: boolean;
}
