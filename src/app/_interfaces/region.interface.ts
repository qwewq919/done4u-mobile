export class IRegion {
  id: number;
  name: string;
  code: string;
  taxRate: number;
  status: string;
  createdAt: Date;
  deleted: boolean;
}
