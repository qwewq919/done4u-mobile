import { IOrder } from './order.interface';
import { IUser } from './user.interface';

export class IChatMessage {
  id?: number;
  message: string;
  from: IUser;
  order: IOrder;
  createdAt: Date;
  readAt: Date;
}
