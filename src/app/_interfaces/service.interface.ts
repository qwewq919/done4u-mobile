export class IService {
  id?: number;
  name: string;
  type: string;
  status: string;
  description: string;
  createdAt: Date;
  deleted: boolean;
  position: number;
}
