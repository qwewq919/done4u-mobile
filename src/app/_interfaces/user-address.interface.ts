import { IRegion } from './region.interface';
import { IUser } from './user.interface';

export class IUserAddress {
  id?: number;
  address: string;
  region: IRegion;
  zip: string;
  status: string;
  lon: number;
  lat: number;
  owner: IUser;
}
