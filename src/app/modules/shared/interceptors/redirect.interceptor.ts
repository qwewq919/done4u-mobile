import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { StorageService } from '../services/storage/storage.service';

@Injectable()
export class RedirectInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private storage: StorageService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next
      .handle(req)
      .pipe(
        tap(
          () => { },
          async (err) => {
            if (err instanceof HttpErrorResponse) {
              if (err.status === 401 && !/^\/auth\//.test(this.router.url)) {
                await this.storage.clear();

                this.router.navigate(
                  ['/', 'auth', 'login'],
                  {
                    queryParams: { returnUrl: /^\/auth\/login/.test(this.router.url) ? '/' : this.router.url },
                    queryParamsHandling: 'merge'
                  }
                );
              }
            }
          }
        )
      );
  }
}
