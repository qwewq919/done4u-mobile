import { Observable, from } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import { StorageService } from '../services/storage/storage.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private storage: StorageService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let headers = req.headers;

    return from(this.storage.get('auth-token'))
      .pipe(
        switchMap(token => {
          if (token && /\/api\/v1/.test(req.url)) {
            headers = headers.set('Authorization', `Bearer ${token}`);
            req = req.clone({ headers });
          }

          return next.handle(req);
        })
      );
  }
}
