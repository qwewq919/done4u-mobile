import { Geolocation } from '@ionic-native/geolocation/ngx';
import { TooltipsModule } from 'ionic-tooltips';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
// import { IonicSelectableModule } from 'ionic-selectable';
// import { FileSaverModule } from 'ngx-filesaver';

import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { environment } from '@environments/environment';
import { Network } from '@ionic-native/network/ngx';
import { IonicModule } from '@ionic/angular';

import { FormErrorComponent } from './components/form-error/form-error.component';
import { HeaderComponent } from './components/header/header.component';
import { PhoneMaskDirective } from './directives/phone-mask.directive';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { RedirectInterceptor } from './interceptors/redirect.interceptor';
import { PriceFormatPipe } from './pipes/price-format.pipe';
import { WINDOW_PROVIDERS } from './providers/window.provider';
import { ChatService } from './services/chat/chat.service';
import { OrdersService } from './services/orders/orders.service';
import { SocketService } from './services/socket/socket.service';
import { ImageService } from './services/image/image.service';

const socketConfig: SocketIoConfig = {
  url: environment.socketUrl,
  options: { }
};

@NgModule({
  declarations: [
    FormErrorComponent,
    HeaderComponent,
    PhoneMaskDirective,
    PriceFormatPipe
  ],

  entryComponents: [],

  exports: [
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    TooltipsModule,
    // IonicSelectableModule,
    // FileSaverModule,

    FormErrorComponent,
    HeaderComponent,
    PhoneMaskDirective,
    PriceFormatPipe,
  ],

  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,
    IonicModule,
    TooltipsModule.forRoot(),
    SocketIoModule.forRoot(socketConfig),
    // IonicSelectableModule,
    // FileSaverModule,
  ],

  providers: [
    WINDOW_PROVIDERS,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RedirectInterceptor,
      multi: true
    },
    OrdersService,
    Network,
    Geolocation,
    ChatService,
    SocketService,
    ImageService,
  ]
})
export class SharedModule {}
