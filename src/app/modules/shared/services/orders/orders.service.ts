import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { IService } from '@interfaces/service.interface';
import { IServicePrice } from '@interfaces/service-price.interface';
import { IOrder } from '@interfaces/order.interface';
import { environment } from '@environments/environment';
import { IUser } from '@interfaces/user.interface';
import { IOrderSchedule } from '@interfaces/orderSсhedule.interface';
import { IOrderPayment } from '@interfaces/order-payment.interface';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  ordersUrl = `${environment.apiUrl}/orders`;
  orderSchedule = `${environment.apiUrl}/orders/schedule`;
  servicesUrl = `${environment.apiUrl}/services`;
  usersUrl = `${environment.apiUrl}/users`;

  constructor(
    private http: HttpClient,
  ) { }

  getServices(): Observable<{ data: IService[], size: number }> {
    return this.http.post<{ data: IService[], size: number }>(`${this.servicesUrl}/list`, {});
  }

  getServicePrices(serviceId: number): Observable<{ data: IServicePrice[], size: number }> {
    return this.http
      .post<{ data: IServicePrice[], size: number }>(`${this.servicesUrl}/prices/list`, {
        filtering: {
          service: {
            id: serviceId,
          },
          deleted: false
        }
      });
  }

  getOrders(clientId: number): Observable<{ data: IOrder[], size: number }> {
    return this.http
      .post<{ data: IOrder[], size: number }>(
        `${this.ordersUrl}/list`,
        {
          filtering: {
            client: {
              id: clientId
            }
          }
        }
      );
  }

  getOrderById(id: number): Observable<IOrder> {
    return this.http.get<IOrder>(`${this.ordersUrl}/${id}`);
  }

  createOrder(data: Partial<IOrder>) {
    return this.http.post<IOrder>(this.ordersUrl, data);
  }

  updateOrder(id: number, data: Partial<IOrder>): Observable<IOrder> {
    return this.http.put<IOrder>(`${this.ordersUrl}/${id}`, data);
  }

  createAddress(userId: number, address: string): Observable<IUser> {
    return this.http.post<IUser>(`${this.usersUrl}/${userId}/address-from-str`, { address });
  }

  getOrderSсheduleById(orderId: number): Observable<IOrderSchedule[]>{
    return this.http.get<IOrderSchedule[]>(`${this.orderSchedule}/${orderId}`);
  }

  getPaymentsByOrder(orderId: number): Observable<IOrderPayment[]> {
    return this.http.get<IOrderPayment[]>(`${this.ordersUrl}/${orderId}/payments`);
  }
}
