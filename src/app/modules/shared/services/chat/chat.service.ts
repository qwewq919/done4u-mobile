import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { IChatMessage } from '@interfaces/chat-message.interface';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  chatUrl = `${environment.apiUrl}/chat`;

  constructor(
    private http: HttpClient,
  ) { }

  getMessages(orderId: number): Observable<IChatMessage[]> {
    return this.http.get<IChatMessage[]>(`${this.chatUrl}/${orderId}`);
  }

  sendMessage(data: Partial<IChatMessage>): Observable<IChatMessage> {
    return this.http.post<IChatMessage>(this.chatUrl, data);
  }
}
