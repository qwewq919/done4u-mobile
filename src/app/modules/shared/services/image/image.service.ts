import { Injectable } from '@angular/core';

@Injectable()
export class ImageService {
  basePath = '/assets/images/';

  services = {
    carWash: 'car-wash-service-icon.svg',
    grassCutting: 'grass-cut-service.svg',
    snowRemoval: 'snow-removal-service.svg',
    cleaning: 'cleaning-service.svg',
  };

  get carWashIcon(): string {
    return `${this.basePath}${this.services.carWash}`;
  }

  get grassCuttingIcon(): string {
    return `${this.basePath}${this.services.grassCutting}`;
  }

  get snowRemovalIcon(): string {
    return `${this.basePath}${this.services.snowRemoval}`;
  }

  get cleaningIcon(): string {
    return `${this.basePath}${this.services.cleaning}`;
  }

  getServiceIcon(serviceName: string): string {
    const lowerCasedName = serviceName.toLocaleLowerCase();

    if (lowerCasedName.indexOf('wash') !== -1) {
      return this.carWashIcon;
    }

    if (lowerCasedName.indexOf('lawn') !== -1) {
      return this.grassCuttingIcon;
    }

    if (lowerCasedName.indexOf('snow') !== -1) {
      return this.snowRemovalIcon;
    }

    if (lowerCasedName.indexOf('cleaning') !== -1) {
      return this.cleaningIcon;
    }
  }
}
