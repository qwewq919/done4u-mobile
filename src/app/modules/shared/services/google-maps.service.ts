/// <reference types="@types/googlemaps" />

import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';

import { environment } from '@environments/environment';
import { Connectivity } from './connectivity.service';

@Injectable({
  providedIn: 'root'
})
export class GoogleMaps {
  mapElement: any;
  pleaseConnect: any;
  map: google.maps.Map;
  mapInitialised = false;
  mapLoaded = false;
  currentMarker: any;

  constructor(
    public connectivityService: Connectivity,
    private geo: Geolocation,
  ) {
    if (typeof google === 'undefined' || typeof google.maps === 'undefined'){
      console.warn('Google maps JavaScript needs to be loaded.');
      this.disableMap();

      if (this.connectivityService.isOnline()){
        // tslint:disable-next-line: no-string-literal
        window['mapInit'] = () => {
          this.mapLoaded = true;
        };

        const script = document.createElement('script');
        script.id = 'googleMaps';

        if (environment.mapsApiKey){
          script.src = `https://maps.google.com/maps/api/js?key=${environment.mapsApiKey}&callback=mapInit&libraries=places`;
        } else {
          script.src = 'https://maps.google.com/maps/api/js?callback=mapInit';
        }

        document.body.appendChild(script);
      }
    }
  }

  init(mapElement: any, pleaseConnect: any): Promise<any> {
    this.mapElement = mapElement;
    this.pleaseConnect = pleaseConnect;
    return this.loadGoogleMaps();
  }

  async loadGoogleMaps() {
    if (this.connectivityService.isOnline()){
      await this.initMap();
      this.enableMap();
    }
    else {
      this.disableMap();
    }

    this.addConnectivityListeners();
  }

  initMap(): Promise<any> {
    return new Promise(async (resolve) => {
      const geo = this.geo.getCurrentPosition({ enableHighAccuracy: true });

      geo.then(position => {
        const latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        const mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(this.mapElement, mapOptions);
        this.mapInitialised = true;
        resolve(true);
      });
    });
  }

  disableMap(): void {
    if (this.pleaseConnect){
      this.pleaseConnect.style.display = 'block';
    }
  }

  enableMap(): void {
    if (this.pleaseConnect){
      this.pleaseConnect.style.display = 'none';
    }
  }

  addConnectivityListeners(): void {
    this.connectivityService.watchOnline().subscribe(() => {
      setTimeout(async () => {
        if (!this.mapInitialised){
          await this.initMap();
        }

        this.enableMap();
      }, 2000);
    });

    this.connectivityService.watchOffline().subscribe(() => {
      this.disableMap();
    });
  }
}
