import { LoadingController } from '@ionic/angular';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  private loading: HTMLIonLoadingElement;
  private isShowing = false;

  constructor(
    private loadingController: LoadingController
  ) {}

  public async presentLoader(): Promise<void> {
    if (!this.isShowing) {
      this.loading = await this.loadingController.create({
        message: 'Loading'
      });

      this.isShowing = true;
      return await this.loading.present();
    } else {
      this.isShowing = true;
    }
  }

  public async dismissLoader(): Promise<void> {
    if (this.loading && this.isShowing) {
      this.isShowing = false;
      await this.loading.dismiss();
    }
  }
}
