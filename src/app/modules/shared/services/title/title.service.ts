import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TitleService extends Title {
  constructor(@Inject(DOCUMENT) doc: Document) {
    super(doc);
  }

  setTitle(title: string) {
    super.setTitle(`${title} | ${environment.titlePrefix}`);
  }
}
