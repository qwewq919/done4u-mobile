import { Socket } from 'ngx-socket-io';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { EventEmitter, Injectable, OnDestroy } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SocketService implements OnDestroy {
  unsubscribe: Subject<any> = new Subject<any>();
  onUpdateDeal: EventEmitter<string> = new EventEmitter<string>();
  onChatMessage: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private socket: Socket
  ) { }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  initSocket() {
    this.onEvent('connect').subscribe(() => console.log('connected'));
    this.onEvent('disconnect').subscribe(() => console.log('disconnected'));
    this.onMessage().subscribe(this.messageHandler.bind(this));
  }

  onMessage(): Observable<any> {
    return this.socket
      .fromEvent('message')
      .pipe(takeUntil(this.unsubscribe));
  }

  onEvent(eventName: string) {
    return this.socket
      .fromEvent(eventName)
      .pipe(takeUntil(this.unsubscribe));
  }

  messageHandler(data: any) {
    switch (data.action) {
      case 'chat_message':
        this.onChatMessage.emit(data.message);
        break;
    }
  }

  send(message: any) {
    this.socket.emit('message', message);
  }
}
