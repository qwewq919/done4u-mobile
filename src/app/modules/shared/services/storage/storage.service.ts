import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  key = 'Done4uClient';

  constructor(
    private storage: Storage
  ) { }

  public async get(key: string, defaultValue: any = null): Promise<any> {
    const value = await this.storage.get(this.key + '_' + key);
    return value || defaultValue;
  }

  public async set(key: string, value: any) {
    return this.storage.set(this.key + '_' + key, value);
  }

  public async remove(key: string) {
    return this.storage.remove(this.key + '_' + key);
  }

  public async clear() {
    return this.storage.clear();
  }
}
