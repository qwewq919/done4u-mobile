import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'priceFormat'
})
export class PriceFormatPipe implements PipeTransform {
  transform(value: number) {
    const currency = new CurrencyPipe('en_CA');
    return currency.transform(value / 100, 'CAD', 'symbol', '.2');
  }
}
