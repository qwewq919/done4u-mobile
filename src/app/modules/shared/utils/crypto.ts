import { AES, enc, mode, format } from 'crypto-js';
import { environment } from '@environments/environment';

export function decrypt(encryptedData: { iv: string; password: string }): string {
  const iv = enc.Hex.parse(encryptedData.iv);
  const key = enc.Utf8.parse(environment.aesKey);

  return AES.decrypt(encryptedData.password, key, {
    iv,
    mode: mode.CBC,
    format: format.Hex
  }).toString(enc.Utf8);
}
