import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@modules/auth/services/auth.service';
import { StorageService } from '@modules/shared/services/storage/storage.service';

type PageTabType =
  | 'new-orders'
  | 'home'
  | 'profile/info'
  | 'profile/addresses'
  | 'chats';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  @Input() useLogout = true;

  backError = null;
  logoSrc = '/assets/images//ssww.png';

  constructor(
    private router: Router,
    private auth: AuthService,
    private storage: StorageService
  ) { }

  isHeaderVisibleFor(currentPage: PageTabType): boolean {
    return this.router.url.includes(currentPage as string);
  }

  logout() {
    this.auth.logout().subscribe(_ => {
      this.storage
        .get('loginMessage')
        .then(msg => {
          if (msg) {
            this.storage.remove('loginMessage');
            this.backError = msg;
          }

          this.router.navigate(['login']);
        });
    });

  }
}
