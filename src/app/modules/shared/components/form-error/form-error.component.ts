import { FormControl } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-form-error',
  templateUrl: './form-error.component.html',
  styleUrls: ['./form-error.component.scss'],
})
export class FormErrorComponent implements OnInit {
  @Input() ctrl: FormControl;

  constructor() { }

  ngOnInit() { }

  get formInputError() {
    if (this.ctrl) {
      if (this.ctrl.touched || this.ctrl.dirty) {
        const errors = this.ctrl.errors;

        for (const errorName in errors) {
          if (errors[errorName]) {
            switch (errorName) {
              case 'required':
                return 'Field is required';
              case 'minlength':
                return `Enter at least ${errors.minlength.requiredLength} characters.`;
              case 'maxlength':
                return `Enter no more than ${errors.maxlength.requiredLength} characters.`;
              case 'email':
                return 'Invalid e-mail';
              case 'pattern':
                return 'Invalid value';
              case 'emailExists':
                return 'E-mail already registered';
              case 'passwordsNotMatching':
                return 'Password and confirmation does not match';
              // custom validation for the international-phone-number component, in this case the field is valid
              case 'phoneEmptyError':
                return 'This phone number does not valid for selected country';
              default:
                return errors[errorName];
            }
          }
        }
      }

      return null;
    }
  }
}
