import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthService } from '../services/auth.service';
import { StorageService } from '@modules/shared/services/storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private storage: StorageService,
    private auth: AuthService
  ) { }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return this.storage
      .get('currentUser')
      .then(currentUser => {
        if (currentUser) {
          if (!this.auth.user$.getValue()) {
            this.auth.user$.next(currentUser);
          }

          return true;
        }

        this.router.navigate(['auth', 'login']);
        return false;
      });
  }
}
