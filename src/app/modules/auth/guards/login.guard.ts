import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { StorageService } from '@modules/shared/services/storage/storage.service';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(
    private router: Router,
    private storage: StorageService
  ) { }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return this.storage
      .get('currentUser', false)
      .then(user => {
        if (!user) {
          return true;
        }

        this.router.navigate(['/']);
        return false;
      });
  }
}
