import { NgxMaskIonicModule } from 'ngx-mask-ionic';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthLoginComponent } from './components/login/login.component';
import { AuthLogoutComponent } from './components/logout/logout.component';
import { AuthSignupComponent } from './components/signup/signup.component';
import { AuthResetComponent } from './components/reset/reset.component';
import { AuthGuard } from './guards/auth.guard';
import { SharedModule } from '@modules/shared/shared.module';
import { LoginGuard } from './guards/login.guard';
import { AuthSocialComponent } from './components/social/social.component';
import { AuthFailureComponent } from './components/failure/failure.component';

@NgModule({
  declarations: [
    AuthLoginComponent,
    AuthResetComponent,
    AuthLogoutComponent,
    AuthSignupComponent,
    AuthSocialComponent,
    AuthFailureComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    IonicModule,
    NgxMaskIonicModule,
    AuthRoutingModule,
    SharedModule
  ],
  providers: [
    AuthGuard,
    LoginGuard,
  ]
})
export class AuthModule { }
