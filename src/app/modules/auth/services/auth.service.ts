import { Platform } from '@ionic/angular';
import { BehaviorSubject, Observable, of, Subject, combineLatest, from, EMPTY } from 'rxjs';
import { takeUntil, tap, switchMap } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';

import { environment } from '@environments/environment';
import { IUser } from '@interfaces/user.interface';
import { StorageService } from '@modules/shared/services/storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnDestroy {
  public isAuth$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public user$: BehaviorSubject<IUser> = new BehaviorSubject<IUser>(null);

  private authUrl = environment.apiUrl + '/auth';
  private unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private http: HttpClient,
    private storage: StorageService,
    private plt: Platform
  ) {
    this.plt.ready().then(() => {
      from(this.storage.get('currentUser', false))
        .pipe(
          takeUntil(this.unsubscribe),
          switchMap(user => {
            this.isAuth$.next(!!user);

            if (!!user) {
              return of(user);
            }

            return of(null);
          })
        )
        .subscribe(user => this.user$.next(user));
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  login(authData: any) {
    const url = `${this.authUrl}/login`;

    return this.http
      .post<any>(url, authData)
      .pipe(
        takeUntil(this.unsubscribe),
        switchMap(data => {
          if (data.user && data.access_token) {
            return combineLatest([
              from(this.storage.set('currentUser', data.user)),
              from(this.storage.set('auth-token', data.access_token))
            ])
            .pipe(
              tap(() => {
                this.user$.next(data.user);
                this.isAuth$.next(true);
              })
            );
          }

          return EMPTY;
        })
      );
  }

  logout() {
    return of(null)
      .pipe(
        takeUntil(this.unsubscribe),
        tap(() => {
          this.storage.clear();
          this.user$.next(null);
          this.isAuth$.next(false);
        })
      );
  }

  reset(authData: any) {
    const url = `${this.authUrl}/reset`;

    return this.http
      .post<any>(url, authData)
      .pipe(takeUntil(this.unsubscribe));
  }

  signup(data: Partial<IUser>): Observable<IUser> {
    const url = `${this.authUrl}/signup`;

    return this.http
      .post<IUser>(url, data)
      .pipe(takeUntil(this.unsubscribe));
  }

  checkEmail(email: string): Observable<boolean> {
    const url = `${this.authUrl}/check`;
    return this.http.post<boolean>(url, { email });
  }

  getUserById(id: string): Observable<IUser> {
    return of(null);
    // return this.http.get<IUser>(`${this.clientsUrl}/${id}`);
  }

  updateProfile(id: string, data: Partial<IUser>): Observable<IUser> {
    return of(null);
    // return this.http.put<IUser>(`${this.clientsUrl}/${id}`, data);
  }
}
