import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthFailureComponent } from './components/failure/failure.component';
import { AuthLoginComponent } from './components/login/login.component';
import { AuthLogoutComponent } from './components/logout/logout.component';
import { AuthResetComponent } from './components/reset/reset.component';
import { AuthSignupComponent } from './components/signup/signup.component';
import { AuthSocialComponent } from './components/social/social.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';

const routes: Routes = [
  { path: 'login', component: AuthLoginComponent, canActivate: [ LoginGuard ] },
  { path: 'reset', component: AuthResetComponent, canActivate: [ LoginGuard ] },
  { path: 'logout', component: AuthLogoutComponent, canActivate: [ AuthGuard ] },
  { path: 'signup', component: AuthSignupComponent, canActivate: [ LoginGuard ] },
  { path: 'failure', component: AuthFailureComponent, canActivate: [ LoginGuard ] },
  { path: 'social/:token', component: AuthSocialComponent, canActivate: [ LoginGuard ] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
