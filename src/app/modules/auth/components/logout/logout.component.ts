import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-auth-logout',
  template: '<div></div>'
})
export class AuthLogoutComponent implements OnInit {
  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.auth
      .logout()
      .subscribe(() => this.router.navigate(['/', 'auth', 'login']));
  }
}
