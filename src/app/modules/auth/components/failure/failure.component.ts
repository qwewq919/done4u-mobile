import { Platform } from '@ionic/angular';
import { pluck } from 'rxjs/operators';

import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { environment } from '@environments/environment';

@Component({
  selector: 'app-auth-failure',
  templateUrl: './failure.component.html',
  styleUrls: ['./failure.component.scss'],
})
export class AuthFailureComponent implements OnInit {
  logoSrc = '/assets/images/logo-green.png';
  siteName = environment.siteName;
  message = '';

  get isMobile() {
    return this.plt.platforms().includes('mobile');
  }

  get isDesktop() {
    return this.plt.platforms().includes('desktop');
  }

  constructor(
    private plt: Platform,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.queryParams
      .pipe(pluck('message'))
      .subscribe(msg => this.message = msg);
  }
}
