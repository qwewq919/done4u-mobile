import { Subject } from 'rxjs';
import { pluck, takeUntil, tap } from 'rxjs/operators';

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '@environments/environment';
import { Platform } from '@ionic/angular';

import { LoaderService } from '@modules/shared/services/loader/loader.service';
import { AuthService } from '../../services/auth.service';

enum Step {
  Get = 'get',
  Set = 'set'
}

@Component({
  selector: 'app-auth-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class AuthResetComponent implements OnInit, OnDestroy {
  resetForm: FormGroup;
  backError = null;
  logoSrc = '/assets/images/ssww.png';
  siteName = environment.siteName;

  step = Step.Get;
  Step = Step;

  token: string;

  showSuccess = false;
  showError = false;
  showPassword = false;
  showPasswordConfirm = false;

  private unsubscribe: Subject<any> = new Subject<any>();

  get isMobile() {
    return this.plt.platforms().includes('mobile');
  }

  get isDesktop() {
    return this.plt.platforms().includes('desktop');
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private loading: LoaderService,
    private auth: AuthService,
    private plt: Platform,
  ) { }

  ngOnInit() {
    this.auth.logout();

    this.route.queryParams
      .pipe(
        takeUntil(this.unsubscribe),
        pluck('resetToken')
      )
      .subscribe((token: string | undefined) => {
        if (token) {
          this.token = token;
          this.step = Step.Set;
        }

        this.formBuild();
      });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  async resetPassword() {
    this.backError = null;

    if (this.resetForm.valid) {
      await this.loading.presentLoader();

      this.auth
        .reset(this.resetForm.getRawValue())
        .pipe(
          tap(
            () => this.loading.dismissLoader(),
            () => this.loading.dismissLoader(),
          )
        )
        .subscribe(
          res => {
            if (this.step === Step.Set) {
              this.router.navigate(['/', 'auth', 'login']);
            } else {
              if (res) {
                this.showSuccess = true;
              } else {
                this.showError = true;
              }
            }
          },
          err => {
            if (err && err.error && err.error.message) {
              this.backError = err.error.message;
            }

            this.step = Step.Get;
            this.formBuild();
          });
    }
  }

  private formBuild() {
    switch (this.step) {
      case Step.Get:
        this.resetForm = this.fb.group({
          email: ['', Validators.compose([Validators.required, Validators.email])],
          step: Step.Get
        });
        break;

      case Step.Set:
        this.resetForm = this.fb.group({
          password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
          passwordConfirm: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
          step: Step.Set,
          token: this.token
        });
        break;
    }
  }
}
