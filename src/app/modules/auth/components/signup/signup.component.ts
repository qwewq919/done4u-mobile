import { isEmpty } from 'lodash';
import { Subject } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { Platform } from '@ionic/angular';

import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { environment } from '@environments/environment';
import { LoaderService } from '@modules/shared/services/loader/loader.service';
import { StorageService } from '@modules/shared/services/storage/storage.service';
import { AuthService } from '../../services/auth.service';
import { confirmPasswordValidator } from './confirm-password-validator';

@Component({
  selector: 'app-auth-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class AuthSignupComponent implements OnInit {
  logoSrc = '/assets/images//ssww.png';
  signupForm: FormGroup;
  backError = null;
  siteName = environment.siteName;
  showPassword = false;

  stepsCnt = 2;
  curStep = 1;

  private unsubscribe: Subject<any> = new Subject<any>();

  get isMobile() {
    return this.plt.platforms().includes('mobile');
  }

  get isDesktop() {
    return this.plt.platforms().includes('desktop');
  }

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private loading: LoaderService,
    private auth: AuthService,
    private storage: StorageService,
    private plt: Platform,
    @Inject(DOCUMENT) private doc: Document,
  ) { }

  ngOnInit() {
    this.auth.logout();
    this.formBuild();
  }

  async signUp() {
    this.backError = null;

    if (this.signupForm.valid) {
      await this.loading.presentLoader();

      const canadaCode = '+1';
      const data = this.signupForm.getRawValue();
      data.phone = data.phone.replace(canadaCode, '');

      await this.auth
        .signup(data)
        .pipe(
          catchError(err => {
            if (err && err.error && err.error.message) {
              this.backError = err.error.message;
            }
            return err;
          })
        )
        .toPromise();

      await this.loading.dismissLoader();

      this.curStep = 1;
      this.signupForm.reset();
      this.router.navigate(['/', 'auth', 'login']);
    }
  }

  private formBuild() {
    this.signupForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],

      confirmPassword: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(6),
          confirmPasswordValidator
        ])
      ],

      // phone: ['', Validators.compose([Validators.required, Validators.pattern(/\+[0-9]+/)])],
      phone: [null, [
        Validators.required,
        Validators.pattern(/^\d+$/),
        Validators.maxLength(10),
        Validators.minLength(10),
    ]],
      zip: '',
      googleId: null,
      okId: null,
      vkId: null,
    });

    this.storage.get('userData').then(data => {
      this.signupForm.get('email')
        .valueChanges
        .pipe(
          takeUntil(this.unsubscribe),
          debounceTime(500),
          distinctUntilChanged(),
        )
        .subscribe(value => {
          if (value) {
            this.auth
              .checkEmail(value)
              .pipe(takeUntil(this.unsubscribe))
              .subscribe(res => {
                if (!res) {
                  this.signupForm.get('email').setErrors({ emailExists: true });
                } else {
                  this.signupForm.get('email').setErrors(null);
                }
              });
          }
        });

      if (!isEmpty(data)) {
        this.signupForm.patchValue(data);
      }
    });
  }

  googleSignUp() {
    this.storage.set('processSignUp', true).then(() => {
      this.doc.location.href = `${environment.apiUrl}/auth/google`;
    });
  }

  fbSignUp() {
    this.storage.set('processSignUp', true).then(() => {
      this.doc.location.href = `${environment.apiUrl}/auth/fb`;
    });
  }
}
