import { Plugins } from '@capacitor/core';
import { Platform } from '@ionic/angular';
import { tap } from 'rxjs/operators';

import { DOCUMENT } from '@angular/common';
import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { environment } from '@environments/environment';
import { LoaderService } from '@modules/shared/services/loader/loader.service';
import { StorageService } from '@modules/shared/services/storage/storage.service';
import { AuthService } from '../../services/auth.service';

const { Keyboard } = Plugins;

@Component({
  selector: 'app-auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class AuthLoginComponent implements OnInit {
  loginForm: FormGroup;
  backError = null;
 
  logoSrc = '/assets/images/ssww.png';
  siteName = environment.siteName;
  showPassword = false;
  showLogo = true;

  get isMobile() {
    return this.plt.platforms().includes('mobile');
  }

  get isDesktop() {
    return this.plt.platforms().includes('desktop');
  }

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private loading: LoaderService,
    private auth: AuthService,
    private plt: Platform,
    private storage: StorageService,
    private cdr: ChangeDetectorRef,
    @Inject(DOCUMENT) private doc: Document,
  ) { }

  ngOnInit() {
    this.auth.logout();
    this.loading.dismissLoader();
    this.formBuild();

    this.storage.get('loginMessage').then(msg => {
      if (msg) {
        this.storage.remove('loginMessage');
        this.backError = msg;
      }
    });

    this.setKeyboardListeners();
  }

  private setKeyboardListeners(): void {
    if (this.plt.is('hybrid')) {
      Keyboard.addListener('keyboardDidShow', () => {
        this.showLogo = false;
        this.cdr.detectChanges();
      });

      Keyboard.addListener('keyboardDidHide', () => {
        this.showLogo = true;
        this.cdr.detectChanges();
      });
    }
  }

  async signIn() {
    this.backError = null;

    if (this.loginForm.valid) {
      await this.loading.presentLoader();

      this.auth
        .login(this.loginForm.getRawValue())
        .pipe(
          tap(
            () => this.loading.dismissLoader(),
            () => this.loading.dismissLoader(),
          )
        )
        .subscribe(
          () => {
            this.router.navigate(['/']);
          },
          err => {
            if (err && err.error && err.error.message) {
              this.backError = err.error.message;
            }
          });
    }
  }

  fbLogin() {
    this.storage.set('processSignIn', true).then(() => {
      this.doc.location.href = `${environment.apiUrl}/auth/fb`;
    });
  }

  googleLogin() {
    this.storage.set('processSignIn', true).then(() => {
      this.doc.location.href = `${environment.apiUrl}/auth/google`;
    });
  }

  getErrorMessage(control: string) {
    return this.loginForm.get(control).hasError('required') ? 'Field is required' :
      this.loginForm.get(control).hasError('email') ? 'Invalid email' :
        this.loginForm.get(control).hasError('pattern') ? 'Invalid value' :
          this.loginForm.get(control).hasError('minlength') ? 'Enter at least 6 characters' :
            '';
  }

  private formBuild() {
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([ Validators.required, Validators.minLength(6) ])],
    });
  }
}
