import { JwtHelperService } from '@auth0/angular-jwt';
import { combineLatest, of, from } from 'rxjs';
import { pluck, switchMap, tap } from 'rxjs/operators';

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { StorageService } from '@modules/shared/services/storage/storage.service';
import { AuthService } from '../../services/auth.service';

interface TokenPayload {
  data?: any;
  id: string;
  login?: string;
  provider?: string;
  remember?: boolean;
  type?: string;
}

@Component({
  selector: 'app-auth-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.scss'],
})
export class AuthSocialComponent implements OnInit {
  createData: any = {};
  processSignUp = false;

  constructor(
    private route: ActivatedRoute,
    private auth: AuthService,
    private storage: StorageService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.params
      .pipe(
        pluck('token'),
        switchMap((token: string) => {
          const helper = new JwtHelperService();
          const data: TokenPayload = helper.decodeToken(token);

          return from(this.storage.get('processSignUp'))
            .pipe(
              switchMap((signUp) => {
                const fromObs = ((data.type && data.type === 'social') || signUp) ? of(null) : from(this.storage.set('auth-token', token));
                return combineLatest([ fromObs, of(signUp) ]);
              }),

              switchMap(([, signUp]) => {
                if (!signUp) {
                  if (data.type && data.type === 'social') {
                    return combineLatest([ from(this.storage.get('currentUser')), of(signUp) ]);
                  } else {
                    return combineLatest([ this.auth.getUserById(data.id), of(signUp) ]);
                  }
                }

                return combineLatest([ of(null), of(signUp) ]);
              }),

              switchMap(([ user, signUp ]) => {
                if (user) {
                  if (data.type && data.type === 'social') {
                    const update = {};
                    update[`${data.provider}Id`] = data.data[`${data.provider}Id`];
                    return combineLatest([ this.auth.updateProfile(user._id, update), of(signUp) ]);
                  } else {
                    return combineLatest([ of(user), of(signUp) ]);
                  }
                } else if (signUp) {
                  if (data.hasOwnProperty('data')) {
                    this.processSignUp = true;
                    this.createData = data.data;
                  }

                  return combineLatest([ of(null), of(signUp) ]);
                }

                return combineLatest([ of(null), of(false) ]);
              })
            );
        }),

        switchMap(([ user, signUp ]) => {
          if (user) {
            return combineLatest([ from(this.storage.set('currentUser', user))
              .pipe(
                tap(() => {
                  this.auth.user$.next(user);
                  this.auth.isAuth$.next(true);
                })
              ),
              of(null)
            ]);
          } else if (signUp) {
            return combineLatest([ of(null), of(this.storage.set('userData', this.createData).then(() => signUp)) ]);
          }

          return combineLatest([ of(null), of(signUp) ]);
        })
      )
      .subscribe(([ user, signUp ]) => {
        if (user) {
          this.storage.get('redirectUrl').then(url => {
            this.storage.remove('processSignIn').then(() => {
              if (url) {
                this.storage.remove('redirectUrl').then(() => this.router.navigateByUrl(url));
              } else {
                this.router.navigate(['/']);
              }
            });
          });
        } else if (signUp) {
          if (this.processSignUp) {
            this.storage.remove('processSignUp').then(() => {
              this.router.navigate(['/', 'auth', 'signup']);
            });
          } else {
            this.storage.set('loginMessage', 'Клиент уже существует. Войдите').then(() => {
              this.storage.remove('processSignUp').then(() => {
                this.router.navigate(['/', 'auth', 'login']);
              });
            });
          }
        } else {
          this.storage.set('loginMessage', 'Клиент не найден').then(() => {
            this.storage.remove('processSignIn').then(() => {
              this.router.navigate(['/', 'auth', 'login']);
            });
          });
        }
      });
  }
}
