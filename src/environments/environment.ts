// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // apiUrl: 'https://app.done4u.ca/api/v1',
  apiUrl: 'http://localhost:3000/api/v1',
  socketUrl: 'http://localhost:3000',
  aesKey: '1iAZ9PFCwxNvv5jvls6lE16EoiQj3m8G',
  titlePrefix: 'Client | Done4u',
  siteName: 'Done4u',
  mapsApiKey: 'AIzaSyBF8I3TypFAOO_YdQejvteHRLUhNlnEBNo',
  stripe: {
    publishKey: 'pk_test_jEM4jsdFihdswU4QDbwmGnnj00BFbWcPqa'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/dist/zone-error';  // Included with Angular CLI.
